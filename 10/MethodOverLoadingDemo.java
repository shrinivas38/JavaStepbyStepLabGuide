public class MethodOverLoadingDemo{ 
    public int findSquare( int iNumber){  
    return ( iNumber * iNumber);  
    } 
 
    //Add an overloaded method that takes a double value  //and returns the square of it 
    public double findSquare( double iNumber){  
    return ( iNumber * iNumber);  
    } 
 
} 