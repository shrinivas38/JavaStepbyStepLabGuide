import java.util.*;
public class CommandLineSorting  {  
public static void main(String args[])  {  
    if (args.length < 6) {    
    System.out.println("Invalid no of arguments –Supply ");   
    System.exit(0);   
    } 
    double[] a = new double[args.length-1];
   for(int i=1;i<a.length;i++)
    {
        a[i-1] = Double.parseDouble(args[i]);
    }
    for(int i=0;i<a.length;i++)
    {
        for (int j = i + 1; j < a.length; j++) 
            {
                if (a[i] > a[j]) 
                {
                    double temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                }
            }
    }
    System.out.print("Ascending Order:");
        for (int i = 0; i < a.length - 1; i++) 
        {
            System.out.print(a[i] + ",");
        }
        System.out.print(a[a.length - 1]);
    }
}