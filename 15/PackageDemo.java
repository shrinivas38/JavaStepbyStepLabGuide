/* This java files contains a class that depicts the concept of   * packages  */ 
 
 
/* Include the package statement because the class should be    part of com.enr.MyPackage */ package Wipro.TT.ProjectZone; 
 
public class PackageDemo  { 
    public static void main(String args[]){ 
    System.out.println("I am part of a package now"); 
    }
} 