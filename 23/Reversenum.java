class Reversenum{
    public static void main(String args[]){
        if(args[1]==""){
             System.out.println("command line arguments have not been entered ");
             System.exit(0);
        }
        if(args.length<10){
             System.out.println("numbers are less then 10 ");
             System.exit(0);
        }
        for (int i=0;i<args.length-1;i++){
            if(isNumeric(args[i])){
                System.out.println("Enter Numbers ");
                System.exit(0);
            }
        }
        
        for(int i = args.length-1; i >= 0; i--) {
            for(int j=args[i].length( )-1; j>=0; j--) {
                System.out.print(args[i].charAt(j));
            }
            System.out.print(" ");  
        }
        
        
    }
    public static boolean isNumeric(String str)
    {
        for (char c : str.toCharArray())
        {
            if (!Character.isDigit(c)) return false;
        }
        return true;
    }
}